Projektname: 
Vokabeltrainer

-

Funktionalität: Der Vokabeltrainer soll ein Set an Vokabeln besitzen um den User abzufragen.

-

Mögliches Problem: Auswahl über welche Vokabeln abgefragt werden sollen.

Mögicher Lösungsweg: Eine extra Datei anlegen um die jeweiligen Daten/Vokabeln aus dieser auszulesen.

-

Bei der Vokabeldatei muss darauf geachtet werden dass eine bestimme Formatierung eingehalten wird.

Diese ist wie folgt einzuhalten:

In der Datei werden das deutsche und das englische Wort per Komma, jedoch ohne Leerzeichen getrennt.

Zusätzlich wird jedes Wort-Paar pro Zeile eingetragen. Beispiel:

tier,animal,

schluessel,key,

buch,book,

oder: tier,animal,schluessel,key,buch,book