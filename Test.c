#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_WORD_SIZE (255)
#define MAX_DICT_SIZE (100)

typedef struct Word {
    int size;
    char letters[MAX_WORD_SIZE];
} Word;

typedef struct Dictonary {
    int size;
    Word words[MAX_DICT_SIZE];
} Dictonary;


int main(int argc, char ** argv) {

    Dictonary dictonary;

    /*Datei oeffnen
      ==========================*/
    FILE * filePtr;
    Word parsedWord;
    char path[255];
	parsedWord.size = 0;
    dictonary.size = 0;

	//Eingabe des Pfads zur Vokabel Datei
    printf("Geben Sie den Pfad fuer die Vokabeldatei an:\n");
    fgets(path, sizeof(path), stdin);

    //Zeilenumbruch von Eingabe entfernen
    char *pos;
    if ((pos=strchr(path, '\n')) != NULL) {
        *pos = '\0';
    }
	
    filePtr = fopen(path, "r");
    if(filePtr == NULL) {
		//Fehlerausgabe bei nicht oeffnen der Datei
        printf("[Error]: Datei konnte nicht geoeffnet werden!");
        exit(EXIT_FAILURE);
    }

    //Fuellen des Arrays
    while(1) {
        int letter = fgetc(filePtr);
        
        if(letter == '\n') continue;

        if(feof(filePtr)) {
            //Letztes Wort überschreiben
            strcpy(dictonary.words[dictonary.size].letters, parsedWord.letters);
            dictonary.size++;
            parsedWord.size = 0;
            break;
        }
        
        if(letter == ',') {
            strcpy(dictonary.words[dictonary.size].letters, parsedWord.letters);
            dictonary.size++;
            parsedWord.size = 0;
            continue;
        }
        
        parsedWord.letters[parsedWord.size] = letter;
        parsedWord.size++;
        parsedWord.letters[parsedWord.size] = '\0';
    }

    fclose(filePtr);
    
    /*Funktion zur Abfrage der Vokabeln 
      ==========================*/
    int rightNumber = 0;
    int index;
    for(index = 0; index < dictonary.size; index++) {
        Word asked;
        Word translation;
        Word input;

        asked = dictonary.words[index];
        translation = dictonary.words[++index];

        printf("Uebersetze: %s\nUebersetzung: ", asked.letters);
        fgets(input.letters, sizeof(input), stdin);

        //Zeilenumbruch von Eingabe entfernen
        char *pos;
        if ((pos=strchr(input.letters, '\n')) != NULL) {
            *pos = '\0';
        }
		
		//Hochz‰hlen der korrekten Anworten
        if(strcmp(input.letters, translation.letters) == 0) {
            printf("Richtig!\n");
            rightNumber++;
        }
    }
	//Ausgabe der Anzahl, korrekt beantworteter Vokabeln
    printf("Richtige Vokabeln: %d\n", rightNumber);

    return EXIT_SUCCESS;
}